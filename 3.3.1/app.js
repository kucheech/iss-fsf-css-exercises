var successCallback = function () {
    console.log("successCallback");
}

var errorCallback = function () {
    console.log("errorCallback");
}

function f(N, a, b) {
    if(N > 18) {
        a();
    } else {
        b();
    }
}

f(2, successCallback, errorCallback);
f(20, successCallback, errorCallback);