'use strict';

const express = require('express');
const app = express();

// greets the user Good morning, Good afternoon, or Good evening depending on time of day
app.get('/', function (req, res) {
    var date = new Date();
    var hour = date.getHours();
    var message = "Good ";
    if(hour < 12) {
        message += "morning";
    } else if(hour < 18) {
        message += "afternoon";
    } else {
        message += "evening";
    }
    res.send(message);
});

// handles server errors (i.e., 5xx) and undefined routes (404s) by sending appropriate 501 and 404 pages



app.listen(3000, function () {
    console.log('MCQ app listening on port 3000!')
})



