//load express module
const express = require("express");

//define a port for the server to listen to
//NODE_PORT taken from the user environment if defined, otherwise to use 3000 instead
const NODE_PORT = process.env.NODE_PORT || 3000;

//creates an instance of the express module
const app = express();

app.use(express.static(__dirname + "/../client/"));

app.get("/about", function (req, res, next) {
    console.log("About ACME");
    next();
});

app.use("/about", function (req, res) {
    res.send("About ACME");
});

app.use(function (req, res, next) {
    res.status(404).sendFile("404.html", {"root": __dirname+"/../client/assets/messages"});
});

app.use(function (err, req, res, next) {
    res.status(501).sendFile("501.html", {"root": __dirname+"/../client/assets/messages"});
});

//Server starts and listens at NODE_PORT
app.listen(NODE_PORT, function () {
    console.log("Server running at http://localhost: " + NODE_PORT);
})