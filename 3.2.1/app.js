(function () {
    //https://www.npmjs.com/package/prompt
    var prompt = require('prompt');

    prompt.start();

    prompt.get("N", function (err, result) {
        var N = result.N;
        var array = [];
        var sum = 0;
        for (var i = 0; i <= N; i++) {
            array.push(i);
            sum += i;
        }

        console.log("array: " + array.toString());
        console.log("sum: " + sum);
    });
})();


