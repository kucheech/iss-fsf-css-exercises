const express = require("express");
const bodyParser= require('body-parser');

const app = express();
const NODE_PORT = process.env.NODE_PORT || 3000;

app.use(express.static(__dirname+"/../client/"));

app.listen(NODE_PORT, function(){
    console.log("Server started at port " + NODE_PORT);
});