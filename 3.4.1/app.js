function Employee(firstName, lastName, employeeNumber, salary, age, department) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.employeeNumber = employeeNumber;
    this.salary = salary;
    this.age = age;
    this.department = department;
    this.getFullName = function () {
        return this.firstName + " " + this.lastName;
    };
    this.isRich = function () {
        return this.salary > 2000;
    }
    this.belongsToDepartment = function (d) {
        return this.department == d;
    }
}